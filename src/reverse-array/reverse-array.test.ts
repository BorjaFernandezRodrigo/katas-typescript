import { ReverseArray } from './reverse-array';
import { describe, test, expect } from '@jest/globals';

describe('Reverse Arrayt', () => {
    test('should reverse arrays', () => {
        const reverseArray = new ReverseArray();
        expect(reverseArray.reverse([1, 2, 3])).toStrictEqual([3, 2, 1]);
        expect(reverseArray.reverse([...'01234567890123456789'])).toStrictEqual([...'98765432109876543210']);
        expect(reverseArray.reverse([0, undefined])).toStrictEqual([undefined, 0]);
    });
});
