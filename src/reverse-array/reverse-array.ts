/* eslint-disable class-methods-use-this */
export class ReverseArray {
    public reverse(array: unknown[]): unknown[] {
        return array.map((_v, index) => array[array.length - 1 - index]);
    }
}
