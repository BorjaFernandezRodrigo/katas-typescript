/* eslint-disable class-methods-use-this */
export class StringEndsWith {
    public comprobate(str: string, ending: string): boolean {
        return str.endsWith(ending);
    }
}
