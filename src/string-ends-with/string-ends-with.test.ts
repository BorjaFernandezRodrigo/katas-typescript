import { StringEndsWith } from './string-ends-with';
import { describe, test, expect } from '@jest/globals';

describe('String ends with', () => {
    test('Si se le pasa parametros correctos debe devolver true', () => {
        const stringEndsWith = new StringEndsWith();
        expect(stringEndsWith.comprobate('abcde', 'cde')).toStrictEqual(true);
    });

    test('Si se le pasa parametros incorrectos debe devolver false', () => {
        const stringEndsWith = new StringEndsWith();
        expect(stringEndsWith.comprobate('abcde', 'abc')).toStrictEqual(false);
    });
});
