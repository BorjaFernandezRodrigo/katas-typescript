# Katas-TypeScript

> Repositorio para almacenar las katas de [codewars](https://www.codewars.com/) para TypeScript

[![](https://www.codewars.com/users/BorjaFernandezRodrigo/badges/small)](https://www.codewars.com/users/BorjaFernandezRodrigo)

[![pipeline status](https://gitlab.com/BorjaFernandezRodrigo/katas-typescript/badges/main/pipeline.svg)](https://gitlab.com/BorjaFernandezRodrigo/katas-typescript/-/commits/main)
[![coverage report](https://gitlab.com/BorjaFernandezRodrigo/katas-typescript/badges/main/coverage.svg)](https://gitlab.com/BorjaFernandezRodrigo/katas-typescript/-/commits/main)

## Indice

-   3 kyu

    -   [The soul of wit: reverse an array](./src/reverse-array/README.md)

-   7 kyu

    -   [String ends with?](./src/string-ends-with/README.md)
