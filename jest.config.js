/** @type {import('ts-jest').JestConfigWithTsJest} */
module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'node',
    resetMocks: false,
    setupFiles: ['jest-localstorage-mock'],
    globals: {
        tsConfig: 'tsconfig.test.json',
    },
    testMatch: ['**/*.test.ts'],
    collectCoverage: true,
    coverageDirectory: './dist/tests/coverage',
    coverageReporters: ['clover', 'json', 'lcov', 'text'],
    collectCoverageFrom: ['src/**/*.ts', '!src/**/*.test.ts', '!node_modules/**', '!dist/**'],
    coverageThreshold: {
        global: {
            branches: 80,
            functions: 80,
            lines: 80,
            statements: -10,
        },
    },
    reporters: [
        'default',
        [
            'jest-junit',
            {
                suiteName: 'report',
                outputDirectory: './dist/tests/report',
                outputName: 'report.xml',
                uniqueOutputName: 'false',
            },
        ],
        [
            'jest-html-reporters',
            {
                publicPath: './dist/tests/report',
                filename: 'report.html',
                openReport: false,
                darkTheme: true,
            },
        ],
    ],
};
